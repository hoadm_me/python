# Python

**Python** là một ngôn ngữ rất tuyệt vời, hiện nay Python đang được sử dụng nhiều trong các lĩnh vực khác nhau. Để bắt đầu với Python, chúng ta sẽ bắt đầu với một số vấn đề:

## Phiên bản
Thời điểm hiện tại, Python đang được duy trình với 2 phiên bản đồng thời là **Python 2.x** và **Python 3.x**. Trong quá trình sử dụng, mình cảm thấy phiên bản 2x được thiết kế tự do gần giống Bash còn phiên bản 3x gần với các ngôn ngữ cấp cao, có hỗ trợ OOP...vv

Tuy nhiên, nhà phát hành Python đã có kế hoạch ngưng phát triển phiên bản 2x và chuyển dần qua Python 3, do đó để tiện lợi chúng ta sẽ bắt đầu với Python 3.

## Download và cài đặt
Việc cài đặt Python cũng cực kỳ dễ dàng, chúng ta chỉ cần truy cập [trang chủ python](https://www.python.org/), chọn menu **Downloads**, chọn phiên bản mới nhất phù hợp với hệ điều hành máy của mình, tải về và tiến hành cài đặt. 

## Text Editor & IDE
Để làm việc với Python, chúng ta có rất nhiều lựa chọn, từ những text editors đơn giản như Sublime Text, Visual Studio Code hay Notepad++ đến những IDE hỗ trợ tận răng như PyCharm. Tùy vào khả năng sử dụng máy tính (các bạn có cảm thấy dễ dàng với Command Line?...vv) các bạn có thể lựa chọn 1 phần mềm hỗ trợ tốt nhất cho mình.

Tuy nhiên, để dễ dàng ghi chú, giải thích một số khái niệm, vấn đề... trong xuyên suốt dự án này, mình sẽ sử dụng [Jupyter Lab](https://jupyter.org/index.html). Jupyter Lab có thể được xem là một Text Editor, môi trường phát triển (development environment), nó tích hợp khá nhiều thứ hay ho để chúng ta có thể giành nhiều thời gian để hiểu về mã nguồn của mình hơn. 

Trong các mục kế tiếp, mình sẽ giới thiệu về cách cài đặt và sử dụng Jupyter Lab, nhưng cho tới lúc đó, chúng ta còn một vài khái niệm khác nữa.

## PIP
Nếu các bạn đã từng học một ngôn ngữ lập trình nào đó, chắc các bạn sẽ không xa lạ với các khái niệm như thư viện (library), gói (package) hay module... Các ngôn ngữ thường chia các chức năng thành các phần nhỏ hơn để dễ dàng quản lý và phát triển. 

Python cũng không ngoại lệ, các module được xây dựng sẵn (built-in) cũng được phân nhỏ vào các gói, ngoài ra còn hàng ngàn tiện ích khác được các nhà phát triển cung cấp, chúng ta có thể dễ dàng cài đặt và sử dụng trong dự án của mình.

[pip](https://pypi.org/project/pip/) là một công cụ dùng để quản lý các module, từ Python 3.4 pip được tích hợp sẵn trong gói cài Python. Nếu bạn quen thuộc với các hệ điều hành họ \*nix, bạn sẽ cảm thấy rất thân thuộc với pip (ví dụ, để cài đặt thư viện pandas, chúng ta sẽ sử dụng lệnh *pip install pandas*)


## Virtual Environment
Như đã giới thiệu ở mục **pip**, khi sử dụng Python chúng ta thường download và sử dụng (nhúng) các thư viện của bên thứ 3 (3rd party libraries) vào dự án của chúng ta. Khi chúng ta làm nhiều dự án, các phiên bản của các thư viện này có thể thay đổi, dẫn đến lỗi, xung đột ...vv

Để tránh việc đó, Python cung cấp cho chúng ta 1 khái niệm gọi là môi trường ảo, bạn có thể hình dung như sau:

* Python mà chúng ta download và cài đặt ban đầu là phiên bản toàn cục (global environment)
* Nếu tất cả các dự án đều sử dụng môi trường toàn cục, sẽ có thể xảy ra xung đột giữa các thư viện hay phiên bản của chúng.
* Chúng ta sẽ copy môi trường toàn cục -> môi trường cục bộ cho một dự án, ở môi trường cục bộ này, chúng ta có thể dễ dàng cài đặt bất cứ thư viện nào mà không sợ ảnh hưởng đến dự án khác

Để tạo và sử dụng môi trường ảo, chúng ta sẽ thực hiện lần lượt các lệnh sau:

`cd PROJECT_FOLDER
python -m venv venv
source venv/bin/activate`

## Jupyter Lab
Tạo sao sử dụng Jupyter Lab? Bản thân mình rất rất thích Jupyter Lab, nó có quá nhiều tiện ích... do đó mình cũng không giải thích quá nhiều. Sau đây là các bước cài đặt Jupyter Lab (mình sử dụng hệ điều hành MacOS, do đó nếu các bạn sử dụng OS khác, các bạn có thể google để làm tương đương)

1. Tạo thư mục dự án
2. Tại thư mục dự án, tạo và kích hoạt môi trường ảo
3. Cài đặt jupyter lab `pip install jupyterlab`
4. Kích hoạt jupyter lab `jupyter-lab`